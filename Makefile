CC=gcc
CFLAGS+=-flto -DUEVENT_USE_FUTEX -O3 -g -std=c11 -pthread -Wall -Wextra
all: test-contention test-single test-latency test-event-contention test-hang

.PHONY: test-contention
test-contention:
	$(CC) -o $@ $(CFLAGS) src/uevent-chooser.c -Iinclude benchmarks/test-contention.c

.PHONY: test-event-contention
test-event-contention:
	$(CC) -o $@ $(CFLAGS) src/uevent-chooser.c -Iinclude benchmarks/test-event-contention.c

.PHONY: test-single
test-single:
	$(CC) -o $@ $(CFLAGS) src/uevent-chooser.c -Iinclude benchmarks/test-single.c

.PHONY: test-latency
test-latency:
	$(CC) -o $@ $(CFLAGS) -lm src/uevent-chooser.c -Iinclude benchmarks/test-latency.c

.PHONY: test-hang
test-hang:
	$(CC) -o $@ $(CFLAGS) -lm src/uevent-chooser.c -Iinclude benchmarks/test-hang.c
