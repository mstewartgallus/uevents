/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define _GNU_SOURCE 1

#include <errno.h>
#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <time.h>

#include "lock.h"
#include "test.h"

#define LOOP 100000U

static void *produce(void *arg);
static void *consume(void *arg);

static struct uevent ready;
static struct uevent done;

static struct timespec signal_start[LOOP];
static struct timespec signal_finish[LOOP];

static void timespec_diff(struct timespec *start, struct timespec *stop,
                          struct timespec *result)
{
	if ((stop->tv_nsec - start->tv_nsec) < 0) {
		result->tv_sec = stop->tv_sec - start->tv_sec - 1;
		result->tv_nsec =
		    stop->tv_nsec - start->tv_nsec + 1000000000;
	} else {
		result->tv_sec = stop->tv_sec - start->tv_sec;
		result->tv_nsec = stop->tv_nsec - start->tv_nsec;
	}
}

int main(int argc, char **argv)
{
	test_setup();

	uevent_init(&ready);
	uevent_init(&done);

	setvbuf(stdout, NULL, _IONBF, 0U);

	pthread_t producer;
	pthread_create(&producer, NULL, produce, NULL);

	pthread_t consumer;
	pthread_create(&consumer, NULL, consume, NULL);

	{
		void *retval;
		pthread_join(producer, &retval);
	}
	{
		void *retval;
		pthread_join(consumer, &retval);
	}

	unsigned long sum = 0;
	unsigned long worst = 0;
	unsigned long best = (unsigned long)-1;
	for (size_t ii = 0U; ii < LOOP; ++ii) {
		struct timespec result;
		timespec_diff(&signal_start[ii], &signal_finish[ii],
		              &result);
		if (result.tv_sec > 0)
			abort();
		sum += result.tv_nsec;
		if (result.tv_nsec > worst)
			worst = result.tv_nsec;
		if (result.tv_nsec < best)
			best = result.tv_nsec;
	}
	double average = sum / (double)LOOP;
	double variation = 0.0;
	for (size_t ii = 0U; ii < LOOP; ++ii) {
		struct timespec result;
		timespec_diff(&signal_start[ii], &signal_finish[ii],
		              &result);
		if (result.tv_sec > 0)
			abort();
		variation += (average - result.tv_nsec) *
		             (average - result.tv_nsec);
	}
	variation /= LOOP - 1UL;
	double sd = sqrt(variation);
	double se = sqrt(variation / LOOP);
	fprintf(stderr, "worst time %lu with sigma %lf \n", worst,
	        (worst - average) / sd);
	fprintf(stderr, "best time %lu with sigma %lf \n", best,
	        (best - average) / sd);
	fprintf(stderr, "standard deviation %lf\n", sd);
	fprintf(stderr, "standard error %lf\n", se);
	fprintf(stderr, "average time %lf\n", average);
	fprintf(stderr, "95%% of the latencies fall within [%lf,%lf] "
	                "(assuming normal distribution)\n",
	        average - 1.96 * se, average + 1.96 * se);

	if (argc <= 1)
		return 0;

	FILE *file = fopen(argv[1], "w");
	if (!file)
		abort();

	fprintf(file, "Latencies\n");
	for (size_t ii = 0U; ii < LOOP; ++ii) {
		struct timespec result;
		timespec_diff(&signal_start[ii], &signal_finish[ii],
		              &result);
		if (result.tv_sec > 0)
			abort();
		sum += result.tv_nsec;
		if (result.tv_nsec > worst)
			worst = result.tv_nsec;
		if (result.tv_nsec < best)
			best = result.tv_nsec;
		fprintf(file, "%lu\n", result.tv_nsec);
	}
	fclose(file);
	return 0;
}

static void *produce(void *arg)
{
	test_setup();

	for (size_t ii = 0U; ii < LOOP; ++ii) {
		clock_gettime(CLOCK_MONOTONIC, &signal_start[ii]);
		uevent_signal(&ready);
		uevent_wait(&done);
		clock_gettime(CLOCK_MONOTONIC, &signal_finish[ii]);
	}
	uevent_signal(&ready);
	return 0;
}

static void *consume(void *arg)
{
	test_setup();

	for (size_t ii = 0U; ii < LOOP; ++ii) {
		uevent_wait(&ready);
		uevent_signal(&done);
	}
	return 0;
}
