/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define _GNU_SOURCE 1

#include <errno.h>
#include <pthread.h>
#include <stdio.h>

#include "lock.h"
#include "test.h"

#define LOOP 100000000U

int main()
{
	test_setup();

	struct lock *lock = lock_create();

	for (size_t ii = 0U; ii < LOOP; ++ii) {
		lock_acquire(lock);
		/* Do nothing */
		lock_release(lock);
	}
	return 0;
}
