/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define _GNU_SOURCE 1

#include <errno.h>
#include <pthread.h>
#include <stdio.h>

#include "lock.h"
#include "test.h"

#define PRODUCER_LOOP 4000000U
#define NUM_THREADS 20U

static void *contend(void *arg);

static struct lock *the_lock;
int main()
{
	test_setup();

	the_lock = lock_create();

	pthread_t threads[NUM_THREADS];
	for (size_t ii = 0U; ii < NUM_THREADS; ++ii) {
		pthread_create(&threads[ii], NULL, contend, NULL);
	}

	for (size_t ii = 0U; ii < NUM_THREADS; ++ii) {
		void *retval;
		pthread_join(threads[ii], &retval);
	}
	return 0;
}

static void *contend(void *arg)
{
	test_setup();

	for (size_t ii = 0U; ii < PRODUCER_LOOP; ++ii) {
		lock_acquire(the_lock);
		/* Do nothing */
		lock_release(the_lock);
	}
	return 0;
}
