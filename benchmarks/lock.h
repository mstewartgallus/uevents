/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef LOCK_H
#define LOCK_H

struct lock;

struct lock *lock_create(void) __attribute__((noinline));
void lock_acquire(struct lock *lock) __attribute__((noinline));
void lock_release(struct lock *lock) __attribute__((noinline));

#if defined USE_NATIVE_LOCKS
#include "lock-native.h"
#elif defined USE_FUTEX_LOCKS
#include "lock-futex.h"
#elif defined USE_SPIN_LOCKS
#include "lock-spin.h"
#else
#include "lock-uevent.h"
#endif
#endif
