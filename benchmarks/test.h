/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef TEST_H_GUARD
#define TEST_H_GUARD
#include <stdlib.h>
#include <sys/prctl.h>

static void test_setup(void)
{
	/* Set the timerslack so that our tasks aren't randomly
	 * delayed. */
	if (-1 == prctl(PR_SET_TIMERSLACK, 1)) {
		abort();
	}
}

#endif
