/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

#include "uevent.h"
#include <linux/futex.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <xmmintrin.h>

struct lock {
	__UEVENT_ALIGN_TO_CACHE _Atomic(_Bool) locked;
	struct uevent when_unlocked;
};

struct lock *lock_create(void)
{
	struct lock *lock = aligned_alloc(UEVENT_ALIGN, sizeof *lock);
	if (!lock)
		abort();
	atomic_store_explicit(&lock->locked, 0, memory_order_relaxed);
	uevent_init(&lock->when_unlocked);
	return lock;
}
void lock_acquire(struct lock *lock)
{
	/* Use two different branches because they have different
	 * branch predictions */
	_Bool expected = 0;
	if (atomic_compare_exchange_strong_explicit(
	        &lock->locked, &expected, 1, memory_order_relaxed,
	        memory_order_relaxed))
		goto acquire;

	for (;;) {
		uevent_wait(&lock->when_unlocked);

		expected = 0;
		if (atomic_compare_exchange_strong_explicit(
		        &lock->locked, &expected, 1,
		        memory_order_relaxed, memory_order_relaxed))
			break;
	}
acquire:
	atomic_thread_fence(memory_order_acquire);
}

void lock_release(struct lock *lock)
{
	atomic_store_explicit(&lock->locked, 0, memory_order_release);
	uevent_signal(&lock->when_unlocked);
}
