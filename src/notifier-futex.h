/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef UEVENT_NOTIFIER_H
#error "notifier-futex.h should only be included by notifier.h"
#endif

struct notifier {
	__UEVENT_ALIGN_TO_CACHE atomic_int triggered;
};

static void notifier_init(struct notifier *notifier)
{
	atomic_store_explicit(&notifier->triggered, 0,
	                      memory_order_release);
}

static void notifier_destroy(struct notifier *notifier)
{
	/* do nothing */
}

static void notifier_signal(struct notifier *notifier)
{
	int expected = 1;
	if (atomic_compare_exchange_strong_explicit(
	        &notifier->triggered, &expected, 2,
	        memory_order_acq_rel, memory_order_acquire))
		return;
	atomic_store_explicit(&notifier->triggered, 2,
	                      memory_order_release);
	syscall(__NR_futex, &notifier->triggered, FUTEX_WAKE_PRIVATE, 1,
	        NULL, NULL, 0);
}

static void notifier_wait(struct notifier *notifier)
{
	for (;;) {
		int expected = 0;
		atomic_compare_exchange_strong_explicit(
		    &notifier->triggered, &expected, 1,
		    memory_order_acq_rel, memory_order_acquire);

		backoff_counter back_count = BACKOFF_INIT;
		for (;;) {
			int expected = 2;
			if (atomic_compare_exchange_strong_explicit(
			        &notifier->triggered, &expected, 0,
			        memory_order_acq_rel,
			        memory_order_acquire))
				goto acquire_wait;
			if (backoff_fail(&back_count))
				break;
		}
		expected = 1;
		if (!atomic_compare_exchange_strong_explicit(
		        &notifier->triggered, &expected, 0,
		        memory_order_acq_rel, memory_order_acquire))
			continue;

		syscall(__NR_futex, &notifier->triggered,
		        FUTEX_WAIT_PRIVATE, 0, NULL, NULL, 0);
	}
acquire_wait:;
}
