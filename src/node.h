/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef UEVENT_NODE_H
#define UEVENT_NODE_H

#include <assert.h>

#include "backoff.h"
#include "notifier.h"

struct notifier;
typedef struct __uevent_node node;

static void node_enqueue(struct uevent *event, node *the_node);
/**
 * Only needs to be single threaded.
 */
static node *node_dequeue(struct uevent *event);

struct __uevent_node {
	struct notifier trigger;
	node *next;
	unsigned char priority;
};

static void node_enqueue(struct uevent *event, node *the_node)
{
	struct __uevent_node *head;
	the_node->next = NULL;

	head =
	    atomic_load_explicit(&event->inbox, memory_order_relaxed);
	backoff_counter back_counter = BACKOFF_INIT;
	for (;;) {
		the_node->next = head;
		if (atomic_compare_exchange_weak_explicit(
		        &event->inbox, &head, the_node,
		        memory_order_release, memory_order_relaxed))
			break;
		back_counter = backoff(back_counter);
	}
}

static inline size_t node_child_left(size_t ii)
{
	return 2U * ii + 1U;
}

static inline size_t node_child_right(size_t ii)
{
	return 2U * ii + 2U;
}

static inline size_t node_parent(size_t ii)
{
	return (ii - 1U) / 2U;
}

static void fixup_heap(struct __uevent_outbox *outbox, size_t ii)
{
	size_t left = node_child_left(ii);
	size_t right = node_child_right(ii);
	size_t largest = ii;

	if (left < outbox->size &&
	    outbox->elems[left]->priority <
	        outbox->elems[largest]->priority) {
		largest = left;
	}
	if (right < outbox->size &&
	    outbox->elems[right]->priority <
	        outbox->elems[largest]->priority) {
		largest = right;
	}

	if (largest != ii) {
		node *iith_node = outbox->elems[ii];
		node *largest_node = outbox->elems[largest];
		outbox->elems[ii] = largest_node;
		outbox->elems[largest] = iith_node;
		fixup_heap(outbox, largest);
	}
}

/*
 * Use Floyd's algorithm for quickly building up a binary tree.
 */
static void outbox_create(struct __uevent_outbox *outbox,
                          node *reversed_linked_list)
{
	assert(0U == outbox->size);

	size_t size = 0U;
	for (node *tail = reversed_linked_list; tail != NULL;
	     tail = tail->next) {
		++size;
	}
	outbox->size = size;

	if (outbox->capacity < size) {
		size_t array_size = size * sizeof outbox->elems[0U];
		if (SIZE_MAX / sizeof outbox->elems[0U] < size)
			abort();

		outbox->elems = realloc(outbox->elems, array_size);
		if (NULL == outbox->elems)
			abort();
		outbox->capacity = size;
	}

	size_t ii = size;
	for (node *tail = reversed_linked_list; tail != NULL;
	     tail = tail->next) {
		--ii;
		outbox->elems[ii] = tail;
	}

	for (size_t ii = size / 2U; ii > 0U; --ii) {
		fixup_heap(outbox, ii);
	}
}

static node *outbox_dequeue(struct __uevent_outbox *outbox)
{
	size_t old_size = outbox->size;
	if (0U == old_size)
		return NULL;
	outbox->size = old_size - 1U;

	node *dequeued = outbox->elems[0U];
	outbox->elems[0U] = outbox->elems[old_size - 1U];

	fixup_heap(outbox, 0U);

	dequeued->next = NULL;

	/* This is needed here because we exploit memory_order_consume
	 * earlier before we reverse and insert the list. */
	atomic_thread_fence(memory_order_acquire);
	return dequeued;
}

static void fetch_from_inbox(struct uevent *event)
{
	/* Test then test and set */
	if (NULL ==
	    atomic_load_explicit(&event->inbox, memory_order_relaxed))
		return;

	/* The first element in the inbox shall the last in the outbox
	 */
	node *inbox = atomic_exchange_explicit(&event->inbox, NULL,
	                                       memory_order_relaxed);
	if (NULL == inbox)
		return;

	atomic_thread_fence(memory_order_consume);

	outbox_create(&event->outbox, inbox);
}

static node *node_dequeue(struct uevent *event)
{
	node *dequeued = outbox_dequeue(&event->outbox);
	if (dequeued != NULL)
		return dequeued;

	/* Only check from the inbox when we don't have stuff in the
	 * outbox to batch up work. */
	fetch_from_inbox(event);

	return outbox_dequeue(&event->outbox);
}
#endif
